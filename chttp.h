#pragma once
#include <unordered_map>
#include <unordered_set>
#include <string_view>
#include <deque>
#include <memory>
#include <regex>


extern "C" {
#include <openssl/ssl.h>
#include <openssl/err.h>
}


namespace lib {
	class chttp
	{
	private:
		class hash_equal_string_view_icase {
		public:
			inline size_t operator ()(const std::string_view& k) const { size_t h{ 2166136261UL }; uint8_t* s{ (uint8_t*)&h }; for (size_t n = 0; n < k.size(); ++n) { s[n % sizeof(h)] = (uint8_t)std::tolower(k[n]); } return h; }
			inline bool operator () (const std::string_view& l, const std::string_view& r) const { return strncasecmp(l.data(), r.data(), std::max(l.length(), r.length())) == 0; }
		};

		class cheaders : protected std::unordered_map<std::string_view, std::string_view, hash_equal_string_view_icase, hash_equal_string_view_icase> {
			using container_t = std::unordered_map<std::string_view, std::string_view, hash_equal_string_view_icase, hash_equal_string_view_icase>;
		public:
			inline std::string_view at(const std::string_view& header, std::string_view def = {}) const { if (auto&& it{ container_t::find(header) }; it != container_t::end()) { return it->second; } return def; }
			inline std::string str(const std::string_view& header) const { return std::string{ at(header) }; }
			size_t number(const std::string_view& header, int base = 10) const { if (auto&& val{ str(header) }; !val.empty()) { try { return std::stoull(val, 0, base); } catch (...) {} } return 0; }
			std::unordered_map<std::string_view, std::string_view> params(const std::string_view& header) const;
			std::unordered_set<std::string_view> explode(const std::string_view& header, const char separator = ',') const;
			bool match(const std::string_view& header, const std::regex& re, std::vector<std::string_view>&) const;
			inline auto emplace(const std::string_view& k, const std::string_view& v) { return  container_t::emplace(k, v); }
			inline auto begin() const { return container_t::begin(); }
			inline auto end() const { return container_t::end(); }
			inline auto size() const { return container_t::size(); }
		};
		class curi {
			class cargs : protected std::unordered_multimap<std::string_view, std::string_view> {
				using container_t = std::unordered_multimap<std::string_view, std::string_view>;
			public:
				inline std::string_view at(const std::string_view& header, std::string_view def = {}) const { if (auto&& it{ container_t::find(header) }; it != container_t::end()) { return it->second; } return def; }
				inline std::string str(const std::string_view& header) const { return std::string{ at(header) }; }
				size_t number(const std::string_view& header, int base = 10) const { if (auto&& val{ str(header) }; !val.empty()) { try { return std::stoull(val, 0, base); } catch (...) {} } return 0; }
				inline auto emplace(const std::string_view& k, const std::string_view& v) { return container_t::emplace(k, v); }
				inline auto begin() const { return container_t::begin(); }
				inline auto end() const { return container_t::end(); }
				inline auto size() const { return container_t::size(); }
				inline auto is(const std::string_view& header) const { return container_t::count(header); }
			};
			class cpath : protected std::deque<std::string_view> {
			public:
				inline std::string_view at(size_t idx) const { if (idx < std::deque<std::string_view>::size()) { return std::deque<std::string_view>::operator[](idx); } return {}; }
				inline std::string str(size_t idx) const { return std::string{ at(idx) }; }
				size_t number(size_t idx, int base = 10) const { if (auto&& val{ str(idx) }; !val.empty()) { try { return std::stoull(val, 0, base); } catch (...) {} } return 0; }
				bool match(size_t arg, const std::regex& re, std::vector<std::string_view>&) const;
				bool match(size_t arg, const std::regex& re, std::string_view&) const;
				bool match(size_t arg, const std::string_view& needle) const;
				inline auto emplace(const std::string_view& k) { return std::deque<std::string_view>::push_back(k); }
				inline auto begin() const { return std::deque<std::string_view>::begin(); }
				inline auto end() const { return std::deque<std::string_view>::end(); }
				inline auto size() const { return std::deque<std::string_view>::size(); }
			};
		public:
			std::string_view schema;
			std::string_view hostport, host, port, url, path, args;
			cpath pathlist;
			cargs argslist;
		};

	public:
		enum class method_t {
			invalid = 0,
			http1 = 0x01, http2 = 0x02, get = 0x04, post = 0x08, put = 0x10, head = 0x20, options = 0x40, del = 0x80, trace = 0x100, connect = 0x200, patch = 0x400,
		};
		using headers_t = cheaders;
		using uri_t = curi;
		using type_t = std::pair<method_t, std::string_view>;
		using ranges_t = std::deque<std::pair<ssize_t, ssize_t>>;
		using headers_list_t = std::unordered_map<std::string, std::string>;

		using response_t = std::pair<std::shared_ptr<uint8_t[]>, size_t>;
		using request_t = std::pair<std::shared_ptr<uint8_t[]>, size_t>;

		ssize_t httpParseRequest(const std::string_view& request, chttp::type_t& type, uri_t& uri, headers_t& headers, std::string_view& payload);
		static ssize_t httpParseResponse(const std::string_view& request, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload);
		ssize_t httpParseRequest(ssize_t fd, const std::string_view& request);
		ssize_t httpParseResponse(ssize_t fd, const std::string_view& request);
		ssize_t httpParseRequest(std::shared_ptr<SSL> fd, const std::string_view& request);
		ssize_t httpParseResponse(std::shared_ptr<SSL> fd, const std::string_view& request);

		static chttp::response_t httpMakeResponse(size_t code, const std::string_view& msg = {}, const headers_list_t& headers = {}, const void* payload = 0, size_t length = 0);
		static chttp::request_t httpMakeRequest(const std::string& method, const std::string& url, const headers_list_t& headers = {}, const void* payload = 0, size_t length = 0);

		static inline chttp::response_t httpMakeResponse(size_t code, const std::string_view& msg, const chttp::headers_list_t& headers, const std::string& payload) {
			return httpMakeResponse(code, msg, headers, payload.data(), payload.size());
		}

		static inline chttp::response_t httpMakeRequest(const std::string& method, const std::string& url, const chttp::headers_list_t& headers, const std::string& payload) {
			return httpMakeRequest(method, url, headers, payload.data(), payload.size());
		}

		static std::string httpFormat(const std::string_view& format, ...);

		static const char* httpMessage(ssize_t code, const std::string_view& msg);
		
		static size_t httpCode(const std::string_view& code);

		bool httpContentLength(const headers_t& headers, size_t& ContentLength);
		bool httpRange(const headers_t& headers, chttp::ranges_t& Ranges);
		std::string httpMakeRangeHeader(std::pair<ssize_t, ssize_t>& ContentRange, size_t ContentLength);

	protected:
		virtual ssize_t httpOnRequest(ssize_t fd, const chttp::type_t& type, const uri_t& uri, const headers_t& headers, const std::string_view& payload) { return 0; }
		virtual ssize_t httpOnResponse(ssize_t fd, const chttp::type_t& type, const std::string_view& code, const std::string_view& msg, const headers_t& headers, const std::string_view& payload) { return 0; }
		virtual ssize_t httpOnRequest(std::shared_ptr<SSL> fd, const chttp::type_t& type, const uri_t& uri, const headers_t& headers, const std::string_view& payload) { return 0; }
		virtual ssize_t httpOnResponse(std::shared_ptr<SSL> fd, const chttp::type_t& type, const std::string_view& code, const std::string_view& msg, const headers_t& headers, const std::string_view& payload) { return 0; }
		virtual ssize_t httpOnError(ssize_t fd, ssize_t err) { return err; }

		ssize_t httpReadResponse(ssize_t fd, size_t max_request_size);
		ssize_t httpReadRequest(ssize_t fd, size_t max_request_size);
		static ssize_t httpReadResponse(ssize_t fd, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, size_t max_request_size);

		ssize_t httpReadResponse(std::shared_ptr<SSL> fd, size_t max_request_size);
		ssize_t httpReadRequest(std::shared_ptr<SSL> fd, size_t max_request_size);
	public:
		static ssize_t httpReadResponse(std::shared_ptr<SSL> fd, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, size_t max_request_size);
		static ssize_t httpRequest(ssize_t fd, const chttp::request_t& request, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, ssize_t maxResponseLength);
		static ssize_t httpRequest(std::shared_ptr<SSL> fd, const chttp::request_t& request, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, ssize_t maxResponseLength);

	};
}