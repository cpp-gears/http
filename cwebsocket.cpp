#include "cwebsocket.h"
#include <cstring> 
#include <sys/epoll.h>
#include <vector>
#include <sys/socket.h>
#include <ctime>
#include "crypto/csha1.h"

using namespace lib;

namespace websocket {

	static inline constexpr uint8_t base64TableEncode[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "abcdefghijklmnopqrstuvwxyz" "0123456789+/";

#pragma pack(push,1)
	union extra_t
	{
		struct small {
		} small;
		struct medium {
			uint16_t len;
		} medium;
		struct large {
			uint64_t len;
		} large;
	};
	using mask_t = uint8_t[4];

	struct frame_t {
		uint8_t opcode : 4;
		uint8_t rsv3 : 1;
		uint8_t rsv2 : 1;
		uint8_t rsv1 : 1;
		uint8_t fin : 1;
		uint8_t len : 7;
		uint8_t mask : 1;
		frame_t() = default;
		frame_t(cwebsocket::opcode_t op,size_t length, bool have_mask = true,bool is_fin = true) :
			opcode{ (uint8_t)((int)op & 0xf) }, rsv3{ 0 }, rsv2{ 0 }, rsv1{ 0 }, fin{ is_fin }, len{ (uint8_t)(length & 0x7f) }, mask{ have_mask }
		{}
	};

	struct small_frame_t : frame_t { mask_t key; uint8_t payload[0];
		small_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,length,true,is_fin } { ; }
	};
	struct small_un_frame_t : frame_t { uint8_t payload[0];
		small_un_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,length,false,is_fin } { ; }
	};
	struct medium_frame_t : frame_t { uint16_t elen; mask_t key; uint8_t payload[0]; 
		medium_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,126,true,is_fin }, elen{ htobe16((uint16_t)length) } {; }
	};
	struct medium_un_frame_t : frame_t { uint16_t elen; uint8_t payload[0];
		medium_un_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,126,false,is_fin }, elen{ htobe16((uint16_t)length) } {; }
	};
	struct large_frame_t : frame_t { uint64_t elen; mask_t key; uint8_t payload[0]; 
		large_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,127,true,is_fin }, elen{ htobe64((uint64_t)length) } {; }
	};
	struct large_un_frame_t : frame_t {
		uint64_t elen; mask_t key; uint8_t payload[0];
		large_un_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,127,false,is_fin }, elen{ htobe64((uint64_t)length) } {; }
	};

#pragma pack(pop)
	static std::string SecKey(const std::string& secKey);
	static void Mask(uint8_t* data, size_t length, const uint8_t* mask);
	static inline void RandMask(mask_t& mask);

	std::string ToBase64(const uint8_t* source, std::size_t length);
}

ssize_t cwebsocket::wsReadResponse(ssize_t fd, size_t max_request_size) {
	return httpReadResponse(fd, max_request_size);
}

ssize_t cwebsocket::wsReadRequest(ssize_t fd, size_t max_request_size) {
	return httpReadRequest(fd, max_request_size);
}

ssize_t cwebsocket::wsWriteMessage(ssize_t fd, opcode_t opcode, const std::string_view& message, bool masked) {
	
	//
	std::vector<uint8_t> data;

	if (message.size() < 126) {
		if (!masked) {
			data.resize(sizeof(websocket::small_un_frame_t) + message.size());
			auto packet = new (data.data()) websocket::small_un_frame_t{ opcode,message.size() };
			std::memcpy(packet->payload, message.data(), message.size());
		}
		else {
			data.resize(sizeof(websocket::small_frame_t) + message.size());
			auto packet = new (data.data()) websocket::small_frame_t{ opcode,message.size() };
			websocket::RandMask(packet->key);
			std::memcpy(packet->payload, message.data(), message.size());
			websocket::Mask(packet->payload, message.size(), packet->key);
		}
	}
	else if (message.size() < 65536) {
		if (!masked) {
			data.resize(sizeof(websocket::medium_un_frame_t) + message.size());
			auto packet = new (data.data()) websocket::medium_un_frame_t{ opcode,message.size() };
			std::memcpy(packet->payload, message.data(), message.size());
		}
		else {
			data.resize(sizeof(websocket::medium_frame_t) + message.size());
			auto packet = new (data.data()) websocket::medium_frame_t{ opcode,message.size() };
			websocket::RandMask(packet->key);
			std::memcpy(packet->payload, message.data(), message.size());
			websocket::Mask(packet->payload, message.size(), packet->key);
		}
	}
	else {
		if (!masked) {
			data.resize(sizeof(websocket::large_un_frame_t) + message.size());
			auto packet = new (data.data()) websocket::large_un_frame_t{ opcode,message.size() };
			std::memcpy(packet->payload, message.data(), message.size());
		}
		else {
			data.resize(sizeof(websocket::large_frame_t) + message.size());
			auto packet = new (data.data()) websocket::large_frame_t{ opcode,message.size() };
			websocket::RandMask(packet->key);
			std::memcpy(packet->payload, message.data(), message.size());
			websocket::Mask(packet->payload, message.size(), packet->key);
		}
	}
	for (size_t noffset{ 0 }, nwrite{ 0 }; noffset < data.size();) {
		if (nwrite = ::send((int)fd, data.data() + noffset, data.size() - noffset, MSG_NOSIGNAL); nwrite > 0) {
			noffset += nwrite;
			continue;
		}
		return wsOnError(fd, -errno);
	}
	return 0;
}

ssize_t cwebsocket::wsReadMessage(ssize_t fd, size_t max_request_size) {
	std::vector<uint8_t> reqData;
	ssize_t noffset{ 0 }, nread{ 0 };
	websocket::frame_t packet;
	websocket::extra_t extra;
	websocket::mask_t mask;
	size_t dataLength{ 0 };

	if (nread = ::recv((int)fd, &packet, sizeof(packet), MSG_NOSIGNAL); nread == sizeof(packet)) {
		if (auto length = packet.len; length < 126) {
			dataLength = length;
		}
		else if (length == 126) {
			if (nread = ::recv((int)fd, &extra, sizeof(websocket::extra_t::medium), MSG_NOSIGNAL); nread != sizeof(websocket::extra_t::medium)) {
				return wsOnError(fd, nread > 0 ? -EMSGSIZE : -errno);
			}
			dataLength = be16toh(extra.medium.len);
		}
		else if (length == 127) {
			if (nread = ::recv((int)fd, &extra, sizeof(websocket::extra_t::large), MSG_NOSIGNAL); nread != sizeof(websocket::extra_t::large)) {
				return wsOnError(fd, nread > 0 ? -EMSGSIZE : -errno);
			}
			dataLength = be64toh(extra.large.len);
		}
		else {
			return wsOnError(fd, -EBADMSG);
		}

		if (packet.mask) {
			if (nread = ::recv((int)fd, &mask, sizeof(websocket::mask_t), MSG_NOSIGNAL); nread != sizeof(websocket::mask_t)) {
				return wsOnError(fd, nread > 0 ? -EMSGSIZE : -errno);
			}
		}

		if (dataLength < max_request_size) {
			reqData.resize(dataLength);
			do {
				if (nread = ::recv((int)fd, reqData.data() + noffset, dataLength - noffset, MSG_NOSIGNAL); nread > 0) {
					noffset += nread;
					if (auto nrest = (dataLength - noffset); nrest > 0) {
						continue;
					}
					else if (!nrest) {
						if (packet.mask) { websocket::Mask(reqData.data(), reqData.size(), mask); }
						return wsMessage(fd, (opcode_t)packet.opcode, std::string_view{ (const char*)reqData.data(),reqData.size() });
					}
					return wsOnError(fd, -EMSGSIZE);
				}
				else if (nread == 0) {
					return wsOnError(fd, -ECONNRESET);
				}
				else if (errno == EAGAIN) {
					if (packet.mask) { websocket::Mask(reqData.data(), reqData.size(), mask); }
					return wsMessage( fd, (opcode_t)packet.opcode, std::string_view{ (const char*)reqData.data(),reqData.size() });
				}
				else {
					wsOnError(fd, -errno);
					break;
				}
			} while (1);
		}
		return wsOnError(fd, -EMSGSIZE);
	}
	else if (nread == 0) {
		return wsOnError(fd, -ECONNRESET);
	}
	return wsOnError(fd, -errno);
}

chttp::request_t cwebsocket::wsMakeRequest(const std::string& uri, const std::string& host, const std::string secKey, const std::string secSubProtocol, const std::string secOrigin, const std::string secVersion) {
	std::string buffer;
	buffer.assign("GET ").append(uri).append(" HTTP/1.1\r\n");
	buffer.append("Host: ").append(host).append("\r\n");
	buffer.append("Upgrade: websocket\r\nConnection: Upgrade\r\n");
	buffer.append("Sec-WebSocket-Key: ").append(secKey).append("\r\n");
	buffer.append("Sec-WebSocket-Version: ").append(secVersion).append("\r\n");
	if (!secOrigin.empty()) { buffer.append("Origin: ").append(secOrigin).append("\r\n");  }
	if (!secSubProtocol.empty()) { buffer.append("Sec-WebSocket-Protocol: ").append(secSubProtocol).append("\r\n"); }
	buffer.append("\r\n");

	chttp::request_t request{ new uint8_t[buffer.length()],buffer.length() };

	if (request.first) {
		std::memcpy(request.first.get(), buffer.data(), buffer.length());
		return request;
	}
	return {};
}

chttp::response_t cwebsocket::wsMakeResponse(const std::string& url, const std::string& secKey, const std::string& secSubProtocol, const std::string& secOrigin, const std::string& secVersion) {
	std::string buffer;
	buffer.assign(
		"HTTP/1.1 101 Switching Protocols\r\n"\
		"Upgrade: WebSocket\r\n"\
		"Connection: Upgrade\r\n"
	);
	buffer.append("Sec-WebSocket-Accept: ").append(websocket::SecKey(secKey)).append("\r\n");
	buffer.append("Sec-WebSocket-Version: ").append(!secVersion.empty() ? secVersion : "13").append("\r\n");
	if (!secSubProtocol.empty()) { buffer.append("Sec-WebSocket-Protocol: ").append(secSubProtocol).append("\r\n"); }

	buffer.append("\r\n");

	chttp::request_t request{ new uint8_t[buffer.length()],buffer.length() };

	if (request.first) {
		std::memcpy(request.first.get(), buffer.data(), buffer.length());
		return request;
	}
	return {};
}

inline void websocket::RandMask(mask_t& mask) {
	srand48(std::time(nullptr));
	*(uint32_t*)mask = (uint32_t)lrand48();
}

void websocket::Mask(uint8_t* data, size_t length, const uint8_t* mask) {
	for (size_t n{ 0 }; n < length; ++n) { data[n] ^= mask[n % 4]; }
}

std::string websocket::SecKey(const std::string& secKey) {

	std::string chain{ secKey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11" };

	hash::csha1::buffer_t hash;
	hash::csha1 digest;
	digest.add(secKey.data(), secKey.size());
	digest.add("258EAFA5-E914-47DA-95CA-C5AB0DC85B11", sizeof("258EAFA5-E914-47DA-95CA-C5AB0DC85B11") - 1);

	return ToBase64(hash.data(), hash.size());
}


std::string websocket::ToBase64(const uint8_t* source, std::size_t length) {
	std::string ret;
	ret.reserve(length * 4);

	int i{ 0 }, j{ 0 };
	uint8_t char_array_3[3], char_array_4[4];

	while (length--) {
		char_array_3[i++] = *(source++);
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & (uint8_t)0xfc) >> 2;
			char_array_4[1] = (uint8_t)(((char_array_3[0] & (uint8_t)0x03) << 4) + ((char_array_3[1] & (uint8_t)0xf0) >> 4));
			char_array_4[2] = (uint8_t)(((char_array_3[1] & (uint8_t)0x0f) << 2) + ((char_array_3[2] & (uint8_t)0xc0) >> 6));
			char_array_4[3] = char_array_3[2] & (uint8_t)0x3f;

			ret.push_back(base64TableEncode[char_array_4[0]]);
			ret.push_back(base64TableEncode[char_array_4[1]]);
			ret.push_back(base64TableEncode[char_array_4[2]]);
			ret.push_back(base64TableEncode[char_array_4[3]]);
			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; char_array_3[j] = '\0', j++);

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = (uint8_t)(((char_array_3[0] & 0x03) << 4) + (uint8_t)((char_array_3[1] & 0xf0) >> 4));
		char_array_4[2] = (uint8_t)(((char_array_3[1] & 0x0f) << 2) + (uint8_t)((char_array_3[2] & 0xc0) >> 6));

		for (j = 0; (j < i + 1); j++) { ret.push_back(base64TableEncode[char_array_4[j]]); }
		while ((i++ < 3)) { ret.push_back('='); }
	}
	ret.shrink_to_fit();
	return ret;
}