#include "chttp.h"
#include <algorithm>
#include <cstdarg>
#include <cstring>
#include <sys/socket.h>
#include <vector>
#include <poll.h>

using namespace lib;

static inline bool parseRequestLine(chttp::method_t& method, const std::string_view& line, std::string_view& type, std::string_view& uri, std::string_view& next_line);
static inline bool parseResponseLine(chttp::method_t& method, const std::string_view& line, std::string_view& type, std::string_view& code, std::string_view& msg, std::string_view& next_line);
static inline bool parseHeaderLine(const std::string_view& line, std::string_view& name, std::string_view& value, std::string_view& nextLine);
static inline void parseUri(const std::string_view& src, chttp::uri_t& uri);
static inline bool parseIsEndLine(std::string_view& line);
static inline std::string_view parseTrimLine(const std::string_view& line);
static inline size_t parseToInt(const std::string_view& value);

static const inline std::regex httpHeaderArgsSplit("([^;=]+)(?:=([^;]+))?");
static const inline std::regex httpUrlSplit(R"(^(?:(.+://)(([^:/\s]*)(?:\:(\d+))?))?(([^?\s]*)(?:\?(\S*))?)?)");

static const struct { std::string_view method; uint64_t value;  uint64_t mask; chttp::method_t type; size_t offset; } 
httpResponsesList[] = {
	{ "HTTP/1", 0x002E002F50545448, 0x00FF00FFFFFFFFFF, chttp::method_t::http1, 9},
	{ "HTTP/2", 0x0020322F50545448, 0x00FFFFFFFFFFFFFF, chttp::method_t::http2, 7},
	{ "",0,0,chttp::method_t::invalid,0},
},
httpRequestMethodsList[] = {
	{ "GET",	0x0000000020544547, 0x00000000FFFFFFFF,chttp::method_t::get,	4},
	{ "POST",	0x0000002054534F50, 0x000000FFFFFFFFFF,chttp::method_t::post,	5},
	{ "PUT",	0x0000000020545550, 0x00000000FFFFFFFF,chttp::method_t::put,	4},
	{ "HEAD",	0x0000002044414548, 0x000000FFFFFFFFFF,chttp::method_t::head,	5},
	{ "OPTIONS",0x20534E4F4954504F, 0xFFFFFFFFFFFFFFFF,chttp::method_t::options,8},
	{ "DELETE", 0x00204554454C4544, 0x00FFFFFFFFFFFFFF,chttp::method_t::del,	7},
	{ "TRACE",	0x0000204543415254, 0x0000FFFFFFFFFFFF,chttp::method_t::trace,	6},
	{ "CONNECT",0x205443454E4E4F43, 0xFFFFFFFFFFFFFFFF,chttp::method_t::connect,8},
	{ "PATCH",	0x0000204843544150, 0x0000FFFFFFFFFFFF,chttp::method_t::patch,	6},
	{ "",0,0,chttp::method_t::invalid,0},
};

bool chttp::curi::cpath::match(size_t arg, const std::regex& re, std::vector<std::string_view>& result) const {
	if (arg < size()) {
		if (auto subject{ at(arg) }; !subject.empty()) {
			try {
				std::cmatch match;
				if (std::regex_match(subject.begin(), subject.end(), match, re) && match.size() > 1) {
					result.reserve(match.size());
					for (size_t n = 0; n < match.size(); ++n) {
						result.emplace_back(match[n].first, match[n].length());
					}
					return true;
				}
			}
			catch (std::regex_error& e) {
			}
		}
	}
	return false;
}

bool chttp::curi::cpath::match(size_t arg, const std::string_view& needle) const {
	if (arg < size()) {
		if (auto subject{ at(arg) }; !subject.empty()) {
			return subject.compare(needle) == 0;
		}
	}
	return false;
}

bool chttp::curi::cpath::match(size_t arg, const std::regex& re, std::string_view& result) const {
	if (arg < size()) {
		if (auto subject{ at(arg) }; !subject.empty()) {
			try {
				std::cmatch match;
				if (std::regex_match(subject.begin(), subject.end(), match, re) && match.size()) {
					result = std::string_view{ match[0].first, (size_t)match[0].length() };
					return true;
				}
			}
			catch (std::regex_error& e) {
			}
		}
	}
	return false;
}

std::unordered_map<std::string_view, std::string_view> chttp::cheaders::params(const std::string_view& header) const {
	std::unordered_map<std::string_view, std::string_view> params;
	if (auto&& it{ find(header) }; it != end()) {
		try {
			auto subject{ parseTrimLine(it->second) };
			std::cregex_iterator next(subject.begin(), subject.end(), httpHeaderArgsSplit);
			std::cregex_iterator end;
			while (next != end) {
				std::cmatch match = *next;
				params.emplace(std::string_view{ match[0].first, (size_t)match[0].length() }, std::string_view{ match[1].first, (size_t)match[1].length() });
				next++;
			}
		}
		catch (std::regex_error& e) {
		}
	}
	return params;
}

std::unordered_set<std::string_view> chttp::cheaders::explode(const std::string_view& header, const char separator) const {
	std::unordered_set<std::string_view> args;
	if (auto&& it{ find(header) }; it != end()) {
		std::size_t start = 0;
		if (auto subject{ parseTrimLine(it->second) }; !subject.empty()) {
			std::size_t end = subject.find(separator);
			while (end != std::string_view::npos)
			{
				args.emplace(parseTrimLine({ subject.data() + start, static_cast<size_t>((subject.data() + end) - (subject.data() + start)) }));
				start = end + 1;
				end = subject.find(separator, start);
			}
			if (auto&& last = parseTrimLine({ subject.data() + start, subject.length() - start }); !last.empty()) {
				args.emplace(last);
			}
		}
	}
	return args;
}

bool chttp::cheaders::match(const std::string_view& header, const std::regex& re, std::vector<std::string_view>& result) const {
	result.clear();
	if (auto&& it{ find(header) }; it != end()) {
		if (auto subject{ parseTrimLine(it->second) }; !subject.empty()) {
			try {
				std::cmatch match;
				if (std::regex_search(subject.begin(), subject.end(), match, re) && match.size() > 1) {
					result.reserve(match.size() - 1);
					for (size_t n = 1; n < match.size(); ++n) {
						result.emplace_back(match[n].first, match[n].length());
					}
					return true;
				}
			}
			catch (std::regex_error& e) {
				// Syntax error in the regular expression
			}
		}
	}
	return false;
}


inline ssize_t readHttpQuery(SSL* fd, uint8_t* buffer, size_t capacity, size_t& nreaded) {
	/* read line by line */
	auto ptr{ buffer }, line{ buffer };
	size_t nContentLength{ 0 };
	ssize_t nread;
	for (; capacity && (nread = SSL_read(fd, ptr, 1)) == 1; ++ptr) {
		capacity--; nreaded++;
		if (*ptr == '\n' and *(ptr - 1) == '\r') {
			/* Compare if endof header */
			if (*(ptr - 3) == '\r' and *(ptr - 2) == '\n') {
				ptr++;
				break;
			}

			/* Compare if Content-Length */
			if ((ptr - line) > 15 && strncasecmp((char*)line, "Content-Length:", 15) == 0) {
				try {
					char* endptr{ nullptr };
					nContentLength = strtoull((char*)line + 15, &endptr, 10);
				}
				catch (...) {
					return -EBADMSG;
				}
			}
			line = ptr + 1;
		}
	}
	
	if (!capacity || !nreaded) return -EMSGSIZE;
	switch (int err = SSL_get_error(fd, (int)nread); err) {
	case SSL_ERROR_NONE:
	case SSL_ERROR_WANT_READ:
	case SSL_ERROR_WANT_WRITE:
		break;
	case SSL_ERROR_ZERO_RETURN:
		return -ECONNABORTED;
		break;
	default:
		return -EBADMSG;
		break;
	}


	if (nContentLength && nContentLength <= capacity) {
		for (; nContentLength && (nread = SSL_read(fd, ptr, (int)nContentLength)) > 0; ptr += nread, nContentLength -= nread) { nreaded += nread; }
		if (nContentLength) return -EBADMSG;
	}
	return 0;
}

ssize_t chttp::httpReadResponse(std::shared_ptr<SSL> fd, size_t max_request_size) {
	std::vector<uint8_t> reqData;
	size_t nread{ 0 };

	reqData.resize(max_request_size);

	if (auto res = readHttpQuery(fd.get(), reqData.data(), reqData.size(), nread); res == 0) {
		return httpParseResponse(fd, std::string_view{ (const char*)reqData.data(), (size_t)nread });
	}
	else {
		return res;
	}
}

ssize_t chttp::httpReadResponse(std::shared_ptr<SSL> fd, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, size_t max_request_size) {
	size_t nread{ 0 };
	response.resize(max_request_size);

	if (auto res = readHttpQuery(fd.get(), response.data(), response.size(), nread); res == 0) {
		return httpParseResponse(std::string_view{ (const char*)response.data(), (size_t)nread }, type, code, msg, headers, payload);
	}
	else {
		return res;
	}
}

ssize_t chttp::httpReadRequest(std::shared_ptr<SSL> fd, size_t max_request_size) {
	std::vector<uint8_t> reqData;
	size_t nread{ 0 };

	reqData.resize(max_request_size);

	if (auto res = readHttpQuery(fd.get(), reqData.data(), reqData.size(), nread); res == 0) {
		return httpParseRequest(fd, std::string_view{ (const char*)reqData.data(), nread });
	}
	else {
		return res;
	}
}

ssize_t chttp::httpReadResponse(ssize_t fd, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, size_t max_request_size) {
	response.resize(max_request_size);
	ssize_t noffset{ 0 }, nread{ 0 };
	do {
		if (nread = ::recv((int)fd, response.data() + noffset, max_request_size - noffset, MSG_NOSIGNAL | MSG_DONTWAIT); nread > 0) {
			noffset += nread;
			if (auto nrest = (max_request_size - noffset); nrest > 0) {
				continue;
			}
			return -ECONNABORTED;
		}
		else if (nread == 0 and errno and errno != EAGAIN) {
			return -errno;
		}
		else if (noffset and (errno == 0 or errno == EAGAIN)) {
			return httpParseResponse(std::string_view{ (const char*)response.data(), (size_t)noffset }, type, code, msg, headers, payload);
		}
		else {
			break;
		}
	} while (1);
	return -errno;
}

ssize_t chttp::httpReadRequest(ssize_t fd, size_t max_request_size) {
	std::vector<uint8_t> reqData;
	reqData.resize(max_request_size);
	ssize_t noffset{ 0 }, nread{ 0 };
	do {
		if (nread = ::recv((int)fd, reqData.data() + noffset, max_request_size - noffset, MSG_NOSIGNAL | MSG_DONTWAIT); nread > 0) {
			noffset += nread;
			if (auto nrest = (max_request_size - noffset); nrest > 0) {
				continue;
			}
			return httpOnError(fd, -ECONNABORTED);
		}
		else if (nread == 0 and errno and errno != EAGAIN) {
			return httpOnError(fd, -errno);
		}
		else if (noffset and (errno == 0 or errno == EAGAIN)) {
			return httpParseRequest(fd, std::string_view{ (const char*)reqData.data(), (size_t)noffset });
		}
		else {
			break;
		}
	} while (1);
	return httpOnError(fd, -errno);
}

ssize_t chttp::httpReadResponse(ssize_t fd, size_t max_request_size) {
	std::vector<uint8_t> reqData;
	reqData.resize(max_request_size);
	ssize_t noffset{ 0 }, nread{ 0 };
	do {
		if (nread = ::recv((int)fd, reqData.data() + noffset, max_request_size - noffset, MSG_NOSIGNAL); nread > 0) {
			noffset += nread;
			if (auto nrest = (max_request_size - noffset); nrest > 0) {
				continue;
			}
			else if (!nrest) {
				return httpParseResponse(fd, std::string_view{ (const char*)reqData.data(), (size_t)noffset });
			}
			httpOnError(fd, -EMSGSIZE);
			return -EMSGSIZE;
		}
		else if (nread == 0) {
			httpOnError(fd, -ECONNRESET);
			return -ECONNRESET;
		}
		else if (errno == EAGAIN) {
			return httpParseResponse(fd,std::string_view{ (const char*)reqData.data(), (size_t) noffset });
		}
		else {
			return httpOnError(fd, -errno);
		}
	} while (1);
	return -errno;
}

chttp::request_t chttp::httpMakeRequest(const std::string& method, const std::string& url, const headers_list_t& headers, const void* payload, size_t length) {

	static auto constexpr query{ "%s %s HTTP/1.1\r\n" };
	static auto constexpr query_payload{ "%s %s HTTP/1.1\r\nContent-Length: %ld\r\n" };

	std::string request;

	if (!length) {
		request.assign(httpFormat(query, method.c_str(), url.c_str()));
		length = 0;
	}
	else {
		request.assign(httpFormat(query_payload, method.c_str(), url.c_str(), length));
	}

	for (auto&& [h, v] : headers) { request.append(h).append(": ").append(v).append("\r\n"); }

	request.append("\r\n");

	request_t data{ new uint8_t[request.length() + (payload ? length : 0)], request.length() + (payload ? length : 0) };

	if (data.first) {
		std::memcpy(data.first.get(), request.data(), request.length());
		if (payload) {
			std::memcpy(data.first.get() + request.length(), payload, length);
		}
		return data;
	}
	return { nullptr,0 };
}

chttp::response_t chttp::httpMakeResponse(size_t code, const std::string_view& msg, const chttp::headers_list_t& headers, const void* payload, size_t length) {
	static auto constexpr answer{ "HTTP/1.1 %ld %s\r\n" };
	//static auto constexpr answer_payload{ "HTTP/1.1 %ld %s\r\nContent-Length: %ld\r\n" };

	std::string response;

	response.assign(httpFormat(answer, code, httpMessage(code, msg)));

	for (auto&& [h, v] : headers) { response.append(h).append(": ").append(v).append("\r\n"); }

	if (payload || length) {
		response.append(httpFormat("Content-Length: %ld\r\n", length));
	}

	response.append("\r\n");

	response_t data{ new uint8_t[response.length() + (payload ? length : 0)],response.length() + (payload ? length : 0) };

	if (data.first) {
		std::memcpy(data.first.get(), response.data(), response.length());
		if (payload) {
			std::memcpy(data.first.get() + response.length(), payload, length);
		}
		return data;
	}
	return { nullptr,0 };
}


std::string chttp::httpFormat(const std::string_view& format, ...) {
	va_list args;
	va_start(args, format);
	size_t size = std::vsnprintf(nullptr, 0, format.data(), args);
	va_end(args);
	if (size) {
		auto buffer{ alloca(size + 1) };
		va_start(args, format);
		std::vsnprintf((char*)buffer, size + 1, format.data(), args);
		va_end(args);
		return { (char*)buffer,size };
	}
	return {};
}

std::string chttp::httpMakeRangeHeader(std::pair<ssize_t,ssize_t>& ContentRange, size_t ContentLength) {
	if (ContentRange.second == 0) {
		ContentRange.second = ContentLength - 1;
	}
	else if (ContentRange.first < 0) {
		ContentRange.first = ContentLength + ContentRange.first;
		ContentRange.second = ContentLength - ContentRange.first;
	}

	ContentRange.second = ContentRange.second - ContentRange.first + 1;

	return "bytes " + std::to_string(ContentRange.first) + "-" + std::to_string(ContentRange.first + ContentRange.second - 1) + "/" + std::to_string(ContentLength);
}

bool chttp::httpRange(const headers_t& headers, chttp::ranges_t& Ranges) {
	Ranges.clear();
	if (std::string_view hvalue{ headers.at("Range") }; !hvalue.empty()) {
		if (std::strncmp(hvalue.data(),"bytes=",6) == 0) {
			ssize_t from = 0, to = 0;
			for (std::string_view crange{ parseTrimLine(hvalue.substr(6)) }; !crange.empty();) {
				from = to = 0;
				if (crange.front() != '-') /* bytes=1-9,... */ {
					for (auto ch : std::string_view(crange)) { if (std::isdigit(ch)) { from = (from * 10) + (ch - '0'); crange.remove_prefix(1); } else if (ch == '-') { crange.remove_prefix(1); break; } else { return false; } }
					for (auto ch : std::string_view(crange)) { if (std::isdigit(ch)) { to = (to * 10) + (ch - '0'); crange.remove_prefix(1); } else if (ch == ',') { crange.remove_prefix(1); break; } else { return false; } }
					Ranges.emplace_back(from, to);
				}
				else /* bytes=-2 get last 2 bytes*/ {
					crange.remove_prefix(1);
					for (auto ch : std::string_view(crange)) { if (std::isdigit(ch)) { from = (from * 10) + (ch - '0'); crange.remove_prefix(1); } else if (ch == ',') { crange.remove_prefix(1); break; } else { return false; } }
					Ranges.emplace_back(-from, 0);
				}
			}
			return true;
		}
	}
	return false;
}

bool chttp::httpContentLength(const headers_t& headers, size_t& ContentLength) {
	ContentLength = 0;
	if (std::string_view hvalue{ headers.at("Content-Length") }; !hvalue.empty()) {
		ContentLength = parseToInt(hvalue);
		return true;
	}
	return false;
}

size_t chttp::httpCode(const std::string_view& code) {
	return parseToInt(code);
}

ssize_t chttp::httpParseRequest(ssize_t fd, const std::string_view& request) {
	
	std::string_view payload;
	chttp::uri_t uri;
	chttp::headers_t headers;
	chttp::type_t type;

	httpParseRequest(request,type, uri, headers, payload);

	return httpOnRequest(fd, type, uri, headers, payload);
}

ssize_t chttp::httpParseResponse(ssize_t fd, const std::string_view& request) {
	std::string_view code, msg, payload;
	chttp::headers_t headers;
	chttp::type_t type;
	httpParseResponse(request, type, code, msg, headers, payload);
	return httpOnResponse(fd, type, code, msg, headers, payload);
}

ssize_t chttp::httpParseRequest(std::shared_ptr<SSL> fd, const std::string_view& request) {

	std::string_view payload;
	chttp::uri_t uri;
	chttp::headers_t headers;
	chttp::type_t type;

	httpParseRequest(request, type, uri, headers, payload);

	return httpOnRequest(fd, type, uri, headers, payload);
}

ssize_t chttp::httpParseResponse(std::shared_ptr<SSL> fd, const std::string_view& request) {
	std::string_view code, msg, payload;
	chttp::headers_t headers;
	chttp::type_t type;
	httpParseResponse(request, type, code, msg, headers, payload);
	return httpOnResponse(fd, type, code, msg, headers, payload);
}


ssize_t chttp::httpParseRequest(const std::string_view& request, chttp::type_t& type, chttp::uri_t& uri, chttp::headers_t& headers, std::string_view& payload) 
{
	std::string_view lines{ request },ruri;

	if (type.first = method_t::invalid; parseRequestLine(type.first, lines, type.second, ruri, lines) && type.first != method_t::invalid) {

		parseUri(ruri, uri);

		while (!parseIsEndLine(lines)) {
			std::string_view k, v;
			parseHeaderLine(lines, k, v, lines);
			headers.emplace(k, v);
		}

		payload = std::string_view{ lines.begin(),static_cast<size_t>(request.end() - lines.begin()) };

		return 0;
	}

	return -EBADMSG;
}

ssize_t chttp::httpParseResponse(const std::string_view& request, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload)
{
	std::string_view lines{ request };

	if (type.first = method_t::invalid; parseResponseLine(type.first, request, type.second, code, msg, lines) && type.first != method_t::invalid) {

		while (!parseIsEndLine(lines)) {
			std::string_view k, v;
			if (parseHeaderLine(lines, k, v, lines)) {
				headers.emplace(k, v);
				continue;
			}
			return -EMSGSIZE;
		}

		payload = std::string_view{ lines.begin(),static_cast<size_t>(request.end() - lines.begin()) };

		return 0;
	}

	return -EBADMSG;
}


ssize_t chttp::httpRequest(ssize_t fd, const chttp::request_t& request, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, ssize_t maxResponseLength) {
	struct pollfd event { .fd = (int)fd, .events = POLLOUT | POLLERR | POLLHUP, .revents = 0 };
	if (poll(&event, 1, 5000) > 0 && (event.revents == POLLOUT)) {
		if(::send((int)fd, request.first.get(), request.second, MSG_NOSIGNAL) == (ssize_t)request.second) {
			event.events = POLLIN | POLLERR | POLLHUP;
			event.revents = 0;
			if (poll(&event, 1, 5000) > 0 && (event.revents == POLLIN)) {
				return httpReadResponse(fd, response, type, code, msg, headers, payload, maxResponseLength);
			}
		}
	}
	return -errno;
}

ssize_t chttp::httpRequest(std::shared_ptr<SSL> fd, const chttp::request_t& request, std::vector<uint8_t>& response, chttp::type_t& type, std::string_view& code, std::string_view& msg, headers_t& headers, std::string_view& payload, ssize_t maxResponseLength) {
	struct pollfd event { .fd = (int)SSL_get_fd(fd.get()), .events = POLLOUT | POLLERR | POLLHUP, .revents = 0 };
	if (poll(&event, 1, 5000) > 0 && (event.revents == POLLOUT)) {
		if (int err = SSL_connect(fd.get()); err == -1) {
			return -SSL_get_error(fd.get(), err);
		}
		if (int len = SSL_write(fd.get(), request.first.get(), (int)request.second); len != (int)request.second) {
			return -SSL_get_error(fd.get(), len);
		}
		event.events = POLLIN | POLLERR | POLLHUP;
		event.revents = 0;

		if (poll(&event, 1, 5000) > 0 && (event.revents == POLLIN)) {
			return httpReadResponse(fd, response, type, code, msg, headers, payload, maxResponseLength);
		}
	}
	return -errno;
}

const char* chttp::httpMessage(ssize_t code, const std::string_view& msg) {
	static const char* zero_code{ "Unknown http error" };
	static const std::unordered_map<ssize_t, const char*> response_codes{
#include "httpcodes.h"
	};
	if (msg.empty()) {
		if (auto&& it = response_codes.find(code); it != response_codes.end()) {
			return it->second;
		}
		return zero_code;
	}
	return msg.data();
}

bool parseRequestLine(chttp::method_t& method, const std::string_view& line, std::string_view& type, std::string_view& uri, std::string_view& next_line) {
	method = chttp::method_t::invalid;
	uint64_t _method{ *(uint64_t*)line.data() };
	for (size_t i = 0; httpRequestMethodsList[i].value; ++i) {
		if ((httpRequestMethodsList[i].mask & _method) == httpRequestMethodsList[i].value) {
			type = line.substr(0, httpRequestMethodsList[i].offset - 1);
			if (auto pos = line.find_first_of(' ', httpRequestMethodsList[i].offset); pos != std::string_view::npos) {
				uri = parseTrimLine(line.substr(httpRequestMethodsList[i].offset, pos - httpRequestMethodsList[i].offset));
				if (pos = line.find_first_of('\n', pos); pos != std::string_view::npos) {
					next_line.remove_prefix(pos + 1);
					method = httpRequestMethodsList[i].type;
					return true;
				}
				else {
					return false;
				}
			}
			break;
		}
	}
	return true;
}

bool parseResponseLine(chttp::method_t& method, const std::string_view& line, std::string_view& type, std::string_view& code, std::string_view& msg, std::string_view& next_line) {
	method = chttp::method_t::invalid;
	uint64_t _method{ *(uint64_t*)line.data() };
	for (size_t i = 0; httpResponsesList[i].value; ++i) {
		if ((httpResponsesList[i].mask & _method) == httpResponsesList[i].value) {
			type = line.substr(0, httpResponsesList[i].offset - 1);
			if (auto pos = line.find_first_of(' ', httpResponsesList[i].offset); pos != std::string_view::npos) {
				code = parseTrimLine(line.substr(httpResponsesList[i].offset, pos - httpResponsesList[i].offset));
				if (!code.empty()) {
					if (auto mpos = line.find('\n', pos); mpos != std::string_view::npos) {
						msg = parseTrimLine(line.substr(pos, mpos - pos));
						next_line.remove_prefix(mpos);
						method = httpResponsesList[i].type;
						return true;
					}
					else {
						return false;
					}
				}
			}
			break;
		}
	}
	return true;
}

bool parseHeaderLine(const std::string_view& line, std::string_view& name, std::string_view& value, std::string_view& next_line) {
	if (auto pos = line.find_first_of(':'); pos != std::string_view::npos) {
		name = parseTrimLine(line.substr(0, pos));
		if (auto npos = line.find_first_of('\n',pos); npos != std::string_view::npos) {
			value = parseTrimLine(line.substr(pos + 1, npos - (pos + 1)));
			next_line.remove_prefix(npos + 1);
			return true;
		}
		return false;
	}
	next_line.remove_prefix(line.length());
	return false;
}

void parseUri(const std::string_view& src, chttp::uri_t& uri) {
	try {
		std::cmatch match;
		if (std::regex_search(src.begin(), src.end(), match, httpUrlSplit) && match.size() > 1) {

			uri.schema = std::string_view{ match[1].first, (size_t)match[1].length() };
			uri.hostport = std::string_view{ match[2].first, (size_t)match[2].length() };
			uri.host = std::string_view{ match[3].first, (size_t)match[3].length() };
			uri.port = std::string_view{ match[4].first, (size_t)match[4].length() };
			uri.url = std::string_view{ match[5].first, (size_t)match[5].length() };
			uri.path = std::string_view{ match[6].first, (size_t)match[6].length() };
			uri.args = std::string_view{ match[7].first, (size_t)match[7].length() };

			if (!uri.path.empty()) {
				std::string_view path{ uri.path };
				if (path.front() == '/' || path.front() == '*') { uri.pathlist.emplace(path.substr(0, 1)); path.remove_prefix(1); }
				for (auto npos = path.find_first_of('/'); npos != std::string_view::npos; path.remove_prefix(npos + 1), npos = path.find_first_of('/')) {
					uri.pathlist.emplace(path.substr(0, npos));
				}
				if (!path.empty()) { uri.pathlist.emplace(path); }
			}

			if (!uri.args.empty()) {
				std::string_view args{ uri.args }, name;
				do {
					if (auto npos = args.find_first_of("=&"); npos != std::string_view::npos) {
						if (args[npos] == '=') {
							name = args.substr(0, npos);
							args.remove_prefix(npos + 1);
						}
						else {
							uri.argslist.emplace(name, args.substr(0, npos));
							args.remove_prefix(npos + 1);
							name = {};
						}
					}
					else {
						if (name.empty()) {
							uri.argslist.emplace(name, {});
						}
						else {
							uri.argslist.emplace(name, args);
						}
						break;
					}
				} while (1);
			}
		}
	}
	catch (std::regex_error& e) {
		// Syntax error in the regular expression
	}
}

bool parseIsEndLine(std::string_view& line) {
	if (*((uint32_t*)(line.begin() - 2)) == uint32_t(0x0a0d0a0d)) {
		line.remove_prefix(2);
		return true;
	}
	else if(*((uint16_t*)(line.begin())) == uint16_t(0x0a0a)){
		line.remove_prefix(1);
		return true;
	}
	return false;
}

std::string_view parseTrimLine(const std::string_view& line) {
	auto  wsfront = std::find_if_not(line.begin(), line.end(), [](int c) {return std::isspace(c); });
	return { wsfront, static_cast<size_t>(std::find_if_not(line.rbegin(), std::string_view::const_reverse_iterator(wsfront), [](int c) {return std::isspace(c); }).base() - wsfront) };
}

size_t parseToInt(const std::string_view& value) {
	size_t _int{ 0 };
	for (auto ch : value) {
		if (std::isdigit(ch)) {
			_int = (_int * 10) + (ch - '0');
		}
	}
	return _int;
}