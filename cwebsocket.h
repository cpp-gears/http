#pragma once
#include "chttp.h"

namespace lib {
	class cwebsocket : private chttp {
	public:
		enum class opcode_t : uint8_t { more = 0x0, text = 0x1, binary = 0x2, close = 0x8, ping = 0x9, pong = 0xa };

		chttp::request_t wsMakeRequest(const std::string& uri, const std::string& host, const std::string secKey, const std::string secSubProtocol = {}, const std::string secOrigin = {}, const std::string secVersion = { "13" });
		chttp::response_t wsMakeResponse(const std::string& url, const std::string& secKey, const std::string& secSubProtocol = {}, const std::string& secOrigin = {}, const std::string& secVersion = { "13" });
	protected:
		ssize_t wsReadResponse(ssize_t fd, size_t max_request_size);
		ssize_t wsReadRequest(ssize_t fd, size_t max_request_size);
		ssize_t wsReadMessage(ssize_t fd, size_t max_request_size);
		ssize_t wsWriteMessage(ssize_t fd, opcode_t opcode, const std::string_view& message, bool masked = false);
		virtual inline ssize_t wsOnError(ssize_t fd, ssize_t err) { return err; }
		virtual inline ssize_t wsRequest(ssize_t fd, const chttp::type_t& type, const uri_t& uri, const headers_t& headers, const std::string_view& payload) { return 0; }
		virtual inline ssize_t wsResponse(ssize_t fd, const chttp::type_t& type, const std::string_view& code, const std::string_view& msg, const headers_t& headers, const std::string_view& payload) { return 0; }
		virtual inline ssize_t wsMessage(ssize_t fd, opcode_t opcode, const std::string_view& message) { return 0; }
	private:
		virtual inline ssize_t httpOnError(ssize_t fd, ssize_t err) { return wsOnError(fd, err); }
		virtual inline ssize_t httpRequest(ssize_t fd, const chttp::type_t& type, const uri_t& uri, const headers_t& headers, const std::string_view& payload) { return wsRequest(fd, type, uri, headers, payload); }
		virtual inline ssize_t httpResponse(ssize_t fd, const chttp::type_t& type, const std::string_view& code, const std::string_view& msg, const headers_t& headers, const std::string_view& payload) { return wsResponse(fd, type, code, msg, headers, payload); }
	};
}